﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using neo4jRentACar.Domain_Models;

namespace neo4jRentACar
{
	public partial class Form1 : Form
	{
		private GraphClient client;
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "baze2020");

			try
			{
				client.Connect();
				Console.WriteLine("Konektovano na bazu");
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox1.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
                listBox1.Items.Add(k.ime);
            }

            var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Klijent) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query2).ToList();

            listBox2.Items.Clear();
            foreach (Klijent k in klijenti)
            {
                listBox2.Items.Add(k.ime + ", " + k.prezime + ", " + k.grad);
            }
        }

		private void button1_Click(object sender, EventArgs e)
		{

            FormKompanija forma3 = new FormKompanija();
            forma3.client = client;
            forma3.ShowDialog();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormRezervacija forma2 = new FormRezervacija();
            forma2.client = client;
            forma2.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox1.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
                listBox1.Items.Add(k.ime);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string t = listBox1.SelectedItem.ToString();
            
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Automobil) where n.kompanija = '" + t + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Automobil> automobili  = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(query).ToList();

            listBoxAutomobili.Items.Clear();
            foreach (Automobil a in automobili)
            {
                listBoxAutomobili.Items.Add(a.marka);
            }

            button2.Enabled = true;
                       
        }






        private void button5_Click(object sender, EventArgs e)
        {
            FormAutomobil f = new FormAutomobil();
            f.client = client;
            f.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FormKlijent f = new FormKlijent();
            f.client = client;
            f.ShowDialog();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex != -1)
            {
                button4.Enabled = true;
            }
        }

     
    }
}
