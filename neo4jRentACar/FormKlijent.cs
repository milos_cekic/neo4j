﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using neo4jRentACar.Domain_Models;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace neo4jRentACar
{
    public partial class FormKlijent : Form
    {
        public GraphClient client;
        public FormKlijent()
        {
            InitializeComponent();
        }

        private void FormKlijent_Load(object sender, EventArgs e)
        {
            var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Klijent) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query2).ToList();

            listBox1.Items.Clear();
            foreach (Klijent k in klijenti)
            {
                listBox1.Items.Add(k.ime + "," + k.prezime + "," + k.grad + "," + k.telefon + "," + k.lk);
            }

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Klijent) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti1 = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query).ToList();

            listBox2.Items.Clear();
            foreach (Klijent k in klijenti1)
            {
                listBox2.Items.Add(k.lk);
            }
        }


        private Klijent dodajKlijenta()
        {
            Klijent k = new Klijent();

            k.ime = textBoxIme.Text;
            k.prezime = textBoxPrezime.Text;
            k.grad = textBoxGrad.Text;
            k.telefon = textBoxTelefon.Text;
            k.lk = textBoxLKarta.Text;
            return k;
        }




        private void button1_Click(object sender, EventArgs e)
        {
            Klijent klijent = this.dodajKlijenta();

            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("ime", klijent.ime);
            dict.Add("prezime", klijent.prezime);
            dict.Add("grad", klijent.grad);
            dict.Add("telefon", klijent.telefon);
            dict.Add("lk", klijent.lk);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Klijent {ime:'" + klijent.ime + "', prezime:'"
                                                                + klijent.prezime + "', grad:'" + klijent.grad+ "', telefon:'" + klijent.telefon + "', lk:'" + klijent.lk + "'}) return n",
                                                                dict, CypherResultMode.Set);

            List<Klijent> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query).ToList();

            textBoxIme.Text = string.Empty;
            textBoxPrezime.Text = string.Empty;
            textBoxLKarta.Text = string.Empty;
            textBoxGrad.Text = string.Empty;
            textBoxTelefon.Text = string.Empty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Klijent) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query2).ToList();

            listBox1.Items.Clear();
            foreach (Klijent k in klijenti)
            {
                listBox1.Items.Add(k.ime + "," + k.prezime + "," + k.grad + "," + k.telefon + "," + k.lk);
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex != -1)
            {
                button4.Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //string tSelect = listBox1.SelectedItems.ToString();
           
            button3.Enabled = true;
            button1.Enabled = false;
            
            string t = listBox1.SelectedItem.ToString();

            string[] tPodeljeno = t.Split(',');

            textBoxIme.Text = tPodeljeno[0].ToString();
            textBoxPrezime.Text = tPodeljeno[1].ToString();
            textBoxGrad.Text = tPodeljeno[2].ToString();
            textBoxTelefon.Text = tPodeljeno[3].ToString();
            textBoxLKarta.Text = tPodeljeno[4].ToString();

            button4.Enabled = false;
            textBoxLKarta.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.Enabled = false;
            button1.Enabled = true;

            Klijent klijent = this.dodajKlijenta();

            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("ime", klijent.ime);
            dict.Add("prezime", klijent.prezime);
            dict.Add("grad", klijent.grad);
            dict.Add("telefon", klijent.telefon);
            dict.Add("lk", klijent.lk);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH(n: Klijent) WHERE n.lk='"+klijent.lk+"' SET n.ime='"+klijent.ime+"'," +
                " n.prezime= '"+klijent.prezime+"', n.grad= '"+klijent.grad+"'," +
                " n.telefon= '"+klijent.telefon+"', n.lk= '"+klijent.lk+"' RETURN n",
                                                                dict, CypherResultMode.Set);

            List<Klijent> klijenti = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query).ToList();

            textBoxIme.Text = string.Empty;
            textBoxPrezime.Text = string.Empty;
            textBoxLKarta.Text = string.Empty;
            textBoxGrad.Text = string.Empty;
            textBoxTelefon.Text = string.Empty;
            textBoxLKarta.Enabled = true;



            //MATCH(n: Klijent { lk: '0089436785432'})
            //SET n.ime = 'Novi', n.prezime = 'Klijent',  n.grad = 'Promenjen grad', n.telefon = 'testtesttest', n.lk = 'nekatamo'
            //RETURN n



        }

        private void button5_Click(object sender, EventArgs e)
        {
            string t = textBox1.Text;

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n { lk:'" + t + "' }) DETACH DELETE n", new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);
//            var result = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query).ToList();
            MessageBox.Show("Uspesno ste obrisali");




        }

        private void button6_Click(object sender, EventArgs e)
        {
            string t = listBox2.SelectedItem.ToString();

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Kompanija)-[r]->(k:Klijent { lk:'" + t + "'} ) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox3.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
                listBox3.Items.Add(k.ime + "," + k.telefon);
            }

            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (k:Klijent { lk:'" + t + "'} )-[r]->(n:Automobil) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Automobil> automobili = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(query2).ToList();

            listBox4.Items.Clear();
            foreach (Automobil a in automobili)
            {
                listBox4.Items.Add(a.marka + ", " + a.model);
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox2.SelectedIndex != -1)
            {
                button6.Enabled = true;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Klijent) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti1 = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query).ToList();

            listBox2.Items.Clear();
            foreach (Klijent k in klijenti1)
            {
                listBox2.Items.Add(k.lk);
            }
        }
    }
}
