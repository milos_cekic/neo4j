﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using neo4jRentACar.Domain_Models;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace neo4jRentACar
{
    public partial class FormKompanija : Form
    {
        public GraphClient client;
        public FormKompanija()
        {
            InitializeComponent();
        }




        private Kompanija napraviKompaniju()
        {
            Kompanija k = new Kompanija();

            k.ime = textBoxIme.Text;
            k.adresa = textBoxAdresa.Text;
            k.telefon = textBoxTelefon.Text;
            return k;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Kompanija kompanija = this.napraviKompaniju();

            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("ime", kompanija.ime);
            dict.Add("adresa", kompanija.adresa);
            dict.Add("telefon", kompanija.telefon);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Kompanija {ime:'" + kompanija.ime + "', adresa:'"
                                                                + kompanija.adresa + "', telefon:'" + kompanija.telefon + "'}) return n",
                                                                dict, CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            textBoxIme.Text = string.Empty;
            textBoxAdresa.Text = string.Empty;
            textBoxTelefon.Text = string.Empty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox1.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
               listBox1.Items.Add(k.ime + ", " + k.adresa + ", " + k.telefon);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string imeKompanije = textBoxBrisiIme.Text;

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Kompanija) WHERE n.ime = '" + imeKompanije + "' DETACH DELETE n", new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH(n: Automobil { kompanija: '" + imeKompanije+ "' }) DETACH DELETE n", new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query2);

            //            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();
           
            MessageBox.Show("Uspesno ste obrisali kompaniju");

            //system("pause");

            button5.Enabled = false;
        }

        private void FormKompanija_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox1.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
                listBox1.Items.Add(k.ime + ", " + k.adresa + ", " + k.telefon);
            }



            var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox2.Items.Clear();
            foreach (Kompanija k in kompanije2)
            {
                listBox2.Items.Add(k.ime);
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button6.Enabled = true;

            string t = listBox1.SelectedItem.ToString();

            string[] tPodeljeno = t.Split(',');

            textBoxIme.Text = tPodeljeno[0].ToString();
            textBoxAdresa.Text = tPodeljeno[1].ToString();
            textBoxTelefon.Text = tPodeljeno[2].ToString();

            button5.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;

            Kompanija kompanija = this.napraviKompaniju();

            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("ime", kompanija.ime);
            dict.Add("adresa", kompanija.adresa);
            dict.Add("telefon", kompanija.telefon);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Kompanija {ime:'" + kompanija.ime + "'  }) SET n.ime = '" + kompanija.ime + "', n.adresa = '" + kompanija.adresa + "', n.telefon = '" + kompanija.telefon + "' RETURN n",
                                                                dict, CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            textBoxIme.Text = string.Empty;
            textBoxAdresa.Text = string.Empty;
            textBoxTelefon.Text = string.Empty;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button5.Enabled = true;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string t = listBox2.SelectedItem.ToString();

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Kompanija { ime: '" + t +
                "' })-[r]->(k:Klijent) return k", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query).ToList();

            listBox3.Items.Clear();
            foreach (Klijent k in klijenti)
            {
                listBox3.Items.Add(k.ime + "," + k.prezime + "," + k.grad + "," + k.telefon + "," + k.lk);
            }
        }
    }
}
