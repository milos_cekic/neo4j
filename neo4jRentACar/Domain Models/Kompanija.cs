﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neo4jRentACar.Domain_Models
{
    public class Kompanija
    {
        public String ime { get; set; }
        public String adresa { get; set; }
        public String telefon { get; set; }

        //public List<Automobil> AutomobiliKompanija { get; set; }
        //public List<Klijent> KlijentiKompanije { get; set; }
        //public List<Rezervacija> RezervacijeKompanije { get; set; }


    }
}