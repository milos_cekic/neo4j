﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neo4jRentACar.Domain_Models
{
    public class Automobil
    {
        public String registracija { get; set; }
        public String marka { get; set; }
        public String model { get; set; }
        public int proizveden { get; set; }
        public String boja { get; set; }
        public String dostupan { get; set; }
        public String kompanija { get; set; }
    }
}