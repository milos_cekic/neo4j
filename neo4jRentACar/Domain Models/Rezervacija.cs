﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neo4jRentACar.Domain_Models
{
    public class Rezervacija
    {
        public String registracijaAuta { get; set; }
        public String od { get; set; }
        public String doDatuma { get; set; }
        public String imeKompanije { get; set; }
        public String imeKlijenta { get; set; }
        public String prezimeKlijenta { get; set; }
        public String lkKlijenta { get; set; }
        public String telefonKlijenta { get; set; }

        //public List<Automobil> AutomobiliRezervacije { get; set; }
    }
} 