﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neo4jRentACar.Domain_Models
{
    public class Klijent
    {
        public String ime { get; set; }
        public String prezime { get; set; }
        public String grad { get; set; }
        public String telefon { get; set; }
        public String lk { get; set; }

        //public List<Automobil> AutomobiliKlijenta { get; set; }
        //public List<Rezervacija> RezervacijeKlijenta { get; set; }

    }
}