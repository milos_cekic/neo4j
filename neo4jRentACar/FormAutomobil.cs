﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using neo4jRentACar.Domain_Models;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace neo4jRentACar
{
    public partial class FormAutomobil : Form
    {
        public GraphClient client;
        public FormAutomobil()
        {
            InitializeComponent();
        }


        private Automobil napraviAutomobil()
        {
            Automobil a = new Automobil();

            a.registracija = textBoxRegistracija.Text;
            a.marka = textBoxMarka.Text;
            a.model = textBoxModel.Text;
            a.proizveden = Int32.Parse(textBoxProizvodnja.Text.ToString());
            a.boja = textBoxBoja.Text;
            a.dostupan = textBoxKompanije.Text;     //ovo je textbox za Dostupan, pogresno je nazvan

            string kompanija = listBox1.SelectedItem.ToString();
                        
            a.kompanija = kompanija;
            
            return a;
        }


        private void FormAutomobil_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox1.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
                listBox1.Items.Add(k.ime);
            }


            //var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            //List<Kompanija> kompanije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query2).ToList();

            //listBox2.Items.Clear();
            //foreach (Kompanija k in kompanije2)
            //{
            //    listBox2.Items.Add(k.ime);
            //}



        }

        private void button1_Click(object sender, EventArgs e)
        {
            //textBoxKompanije.Text = listBox1.SelectedItem.ToString();

            //string t = textBoxKompanije.Text;

            Automobil automobil = this.napraviAutomobil();

            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("registracija", automobil.registracija);
            dict.Add("marka", automobil.marka);
            dict.Add("model", automobil.model);
            dict.Add("proizveden", automobil.proizveden);
            dict.Add("boja", automobil.boja);
            dict.Add("dostupan", automobil.dostupan);
            dict.Add("kompanija", automobil.kompanija);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Automobil {registracija:'" + automobil.registracija + "', marka:'"
                                                                + automobil.marka + "', model:'" + automobil.model + "', proizveden:'" + automobil.proizveden +
                                                                "', boja:'" + automobil.boja + "', dostupan:'" + automobil.dostupan + "', kompanija:'" + automobil.kompanija + "'}) return n",
                                                                dict, CypherResultMode.Set);

            
            List<Automobil> automobili = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(query).ToList();

            //prvo napravimo automobil pa ga onda nadjemo zbog dodavanja veze


            //match (n:Automobil) where n.marka='rrrrrrrr' and n.kompanija='Krusik' match(k:Kompanija) where k.ime='Krusik'
            //CREATE
            //(k) -[r: POSEDUJE]->(n) return r

            var queryVeza = new Neo4jClient.Cypher.CypherQuery("match (n:Automobil) where n.marka='" + automobil.marka + "' and n.kompanija='" + automobil.kompanija + "'"
                                                                + " match(k:Kompanija) where k.ime='" + automobil.kompanija + "' CREATE (k) -[r: POSEDUJE]->(n) return r",
                                                                dict, CypherResultMode.Set);

            List<Automobil> automobili2 = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(queryVeza).ToList();

            textBoxRegistracija.Text = string.Empty;
            textBoxMarka.Text = string.Empty;
            textBoxModel.Text = string.Empty;
            textBoxProizvodnja.Text = string.Empty;
            textBoxBoja.Text = string.Empty;
            textBoxKompanije.Text = string.Empty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        //private void button3_Click(object sender, EventArgs e)
        //{
        //    string t = listBox2.SelectedItem.ToString();

        //    var query = new Neo4jClient.Cypher.CypherQuery("match (n:Automobil) where n.kompanija = '" + t + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

        //    List<Automobil> automobili = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(query).ToList();

        //    listBox3.Items.Clear();
        //    foreach (Automobil a in automobili)
        //    {
        //        listBox3.Items.Add(a.registracija + ", " + a.marka + ", " + a.model + ", " + a.proizveden + ", " + a.boja + ", " + a.dostupan + ", " + a.kompanija);
        //    }
        //}

        //private void button4_Click(object sender, EventArgs e)
        //{
        //    string t =listBox2.SelectedItems.ToString();
            
        //    MessageBox.Show(t.ToString());
        //    string[] tPodeljeno = t.Split(',');
        //    MessageBox.Show(tPodeljeno[1].ToString());
        //    MessageBox.Show(tPodeljeno[2].ToString());

        //    //var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Kompanija) WHERE n.ime = '" + imeKompanije + "' DETACH DELETE n", new Dictionary<string, object>(), CypherResultMode.Set);

        //    //List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

        //    //foreach (Kompanija k in kompanije)
        //    //{
        //    //    MessageBox.Show(k.ime);
        //    //}
        //}

        private void button6_Click(object sender, EventArgs e)
        {
            string t = listBox1.SelectedItem.ToString();

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Automobil) where n.kompanija = '" + t + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Automobil> automobili = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(query).ToList();

            listBox4.Items.Clear();
            foreach (Automobil a in automobili)
            {
                listBox4.Items.Add(a.registracija + ", " + a.marka + ", " + a.model + ", " + a.proizveden + ", " + a.boja + ", " + a.dostupan + ", " + a.kompanija);
            }











        }

        private void button7_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button8.Enabled = true;

            string t = listBox4.SelectedItem.ToString();

            string[] tPodeljeno = t.Split(',');

            textBoxRegistracija.Text = tPodeljeno[0].ToString();
            textBoxMarka.Text = tPodeljeno[1].ToString();
            textBoxModel.Text = tPodeljeno[2].ToString();
            textBoxProizvodnja.Text = tPodeljeno[3].ToString();
            textBoxBoja.Text = tPodeljeno[4].ToString();
            textBoxKompanije.Text = tPodeljeno[5].ToString();

            button7.Enabled = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = true;
            //button3.Enabled = true;

            Automobil automobil = this.napraviAutomobil();

            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("registracija", automobil.registracija);
            dict.Add("marka", automobil.marka);
            dict.Add("model", automobil.model);
            dict.Add("proizveden", automobil.proizveden);
            dict.Add("boja", automobil.boja);
            dict.Add("dostupan", automobil.dostupan);
            dict.Add("kompanija", automobil.kompanija);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Automobil {registracija:'" + automobil.registracija + "'  }) SET n.registracija = '" + automobil.registracija + "', n.marka = '" + automobil.marka + "', n.model = '" + automobil.model + "', n.proizveden = " + automobil.proizveden + ", n.boja = '" + automobil.boja + "', n.dostupan = '" + automobil.dostupan + "' RETURN n",
                                                                dict, CypherResultMode.Set);

            List<Automobil> automobili = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(query).ToList();

            textBoxRegistracija.Text = string.Empty;
            textBoxMarka.Text = string.Empty;
            textBoxModel.Text = string.Empty;
            textBoxKompanije.Text = string.Empty;          textBoxBoja.Text = string.Empty;
            textBoxProizvodnja.Text = string.Empty;
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string t = textBox1.Text;

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Automobil { registracija:'" + t + "' }) DETACH DELETE n", new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            MessageBox.Show("Automobil je obrisan");
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button7.Enabled = true;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button1.Enabled = true;
                button6.Enabled = true;
            }
        }
    }
}
