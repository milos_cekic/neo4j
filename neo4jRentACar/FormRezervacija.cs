﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using neo4jRentACar.Domain_Models;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace neo4jRentACar
{
    public partial class FormRezervacija : Form
    {
        public GraphClient client;
        public FormRezervacija()
        {
            InitializeComponent();
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox1.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
                listBox1.Items.Add(k.ime);
                //MessageBox.Show(k.ime);
            }
        }

        private void FormRezervacija_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(query).ToList();

            listBox1.Items.Clear();
            foreach (Kompanija k in kompanije)
            {
                listBox1.Items.Add(k.ime);
            }



            //MessageBox.Show(DateTime.Now.ToString("dd/MMM/yyyy"));

            var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Rezervacija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Rezervacija> rezervacije = ((IRawGraphClient)client).ExecuteGetCypherResults<Rezervacija>(query2).ToList();

            listBox2.Items.Clear();
            foreach (Rezervacija r in rezervacije)
            {
                listBox2.Items.Add(r.registracijaAuta + "," + r.od + "," + r.doDatuma + "," + r.imeKompanije + "," + r.imeKlijenta + "," + r.prezimeKlijenta + "," + r.lkKlijenta + "," + r.telefonKlijenta);
            }

            var query3 = new Neo4jClient.Cypher.CypherQuery("match (n:Klijent) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(query3).ToList();

            listBox4.Items.Clear();
            foreach (Klijent k in klijenti)
            {
                listBox4.Items.Add(k.ime + "," + k.prezime + "," + k.grad + "," + k.telefon + "," + k.lk);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string t = listBox1.SelectedItem.ToString();

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Automobil) where n.kompanija = '" + t + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Automobil> automobili = ((IRawGraphClient)client).ExecuteGetCypherResults<Automobil>(query).ToList();

            listBox3.Items.Clear();
            foreach (Automobil a in automobili)
            {
                listBox3.Items.Add(a.registracija + "," + a.marka + "," + a.model + "," + a.proizveden + "," + a.boja + "," + a.dostupan + "," + a.kompanija);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Rezervacija) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Rezervacija> rezervacije = ((IRawGraphClient)client).ExecuteGetCypherResults<Rezervacija>(query2).ToList();

            listBox2.Items.Clear();
            foreach (Rezervacija r in rezervacije)
            {
                listBox2.Items.Add(r.registracijaAuta + "," + r.od + "," + r.doDatuma + "," + r.imeKompanije + "," + r.imeKlijenta + "," + r.prezimeKlijenta + "," + r.lkKlijenta + "," + r.telefonKlijenta);
            }
        }

        private Rezervacija napraviRezervaciju()
        {
            Rezervacija r  = new Rezervacija();


            return r;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Klijent klijent = this.napraviRezervaciju();
            

            string t = listBox4.SelectedItem.ToString();
            string[] tPodeljeno = t.Split(',');
            string imeKlijenta = tPodeljeno[0].ToString();
            string prezimeKlijenta = tPodeljeno[1].ToString();
            string telefonKlijenta = tPodeljeno[3].ToString();
            string lkKlijenta = tPodeljeno[4].ToString();

            string t1 = listBox3.SelectedItem.ToString();
            string[] tPodeljeno1 = t1.Split(',');
            string registracijaAutomobila = tPodeljeno1[0].ToString();
            string kompanijaAutomobila = tPodeljeno1[6].ToString();

            string rezervacijaOd = textBox1.Text;
            string rezervacijaDo = textBox2.Text;

            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("doDatuma", rezervacijaDo);
            dict.Add("imeKlijenta", imeKlijenta);
            dict.Add("prezimeKlijenta", prezimeKlijenta);
            dict.Add("imeKompanije", kompanijaAutomobila);
            dict.Add("lkKlijenta", lkKlijenta);
            dict.Add("od", rezervacijaOd);
            dict.Add("registracijaAuta", registracijaAutomobila);


            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Rezervacija {registracijaAuta:'" + registracijaAutomobila + 
                "', od:'"  + rezervacijaOd + "', doDatuma:'" + rezervacijaDo + "', imeKompanije:'" + kompanijaAutomobila + "', imeKlijenta:'" + imeKlijenta + "', prezimeKlijenta:'"
                + prezimeKlijenta + "', lkKlijenta:'" + lkKlijenta + "', telKlijenta:'" + telefonKlijenta + "'}) return n",
                                                                dict, CypherResultMode.Set);

            List<Rezervacija> rezervacije = ((IRawGraphClient)client).ExecuteGetCypherResults<Rezervacija>(query).ToList();


            var queryVezaIzvrsava = new Neo4jClient.Cypher.CypherQuery("match (n:Rezervacija{lkKlijenta:'" 
                + lkKlijenta + "', registracijaAuta:'" + registracijaAutomobila + 
                "', imeKompanije:'" + kompanijaAutomobila + 
                "', doDatuma:'" + rezervacijaDo + "'}) match(k:Automobil{ registracija:'" 
                + registracijaAutomobila + "', kompanija:'" + 
                kompanijaAutomobila + "'})CREATE(n)-[r:Izvrsava]->(k) return n",
                                                                        dict, CypherResultMode.Set);

            List<Rezervacija> rezervacije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<Rezervacija>(queryVezaIzvrsava).ToList();

            var queryVezaRezervise = new Neo4jClient.Cypher.CypherQuery("match (k:Klijent{lk:'" + lkKlijenta + "'}) match(n: Rezervacija{ lkKlijenta:'" + lkKlijenta + "', registracijaAuta: '" + registracijaAutomobila  + "', imeKompanije: '" + kompanijaAutomobila 
                                                                        + "', doDatuma: '" + rezervacijaDo + "'}) CREATE (k)-[r: Rezervise]->(n) return r",
                                                                        dict, CypherResultMode.Set);

            List<Rezervacija> rezervacije3 = ((IRawGraphClient)client).ExecuteGetCypherResults<Rezervacija>(queryVezaRezervise).ToList();

            var queryVezaPravi = new Neo4jClient.Cypher.CypherQuery("match (n:Rezervacija{lkKlijenta:'" + lkKlijenta + "', registracijaAuta:'" + registracijaAutomobila + "', imeKompanije:'" + kompanijaAutomobila + "', doDatuma:'" + rezervacijaDo 
                                                                    + "'}) match(k: Kompanija{ ime:'" + kompanijaAutomobila + "'}) CREATE (n)-[r: Pravi]->(k) return r",
                                                                        dict, CypherResultMode.Set);

            List<Rezervacija> rezervacije4 = ((IRawGraphClient)client).ExecuteGetCypherResults<Rezervacija>(queryVezaPravi).ToList();


            var querySaradjivao = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Kompanija { ime: '"+kompanijaAutomobila+
                "' })-[r]->(k:Klijent{lk:'"+lkKlijenta+"'}) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Kompanija> kompanije = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(querySaradjivao).ToList();

            if (kompanije.Count == 0)
            {
                var queryCreateSaradjivao = new Neo4jClient.Cypher.CypherQuery("match (n:Kompanija{ime:'"
                               + kompanijaAutomobila + "'}) match(k:Klijent{lk:'"
                               + lkKlijenta + "'})CREATE(n)-[r:Saradjivao]->(k) return n",
                    dict, CypherResultMode.Set);

                List<Kompanija> komp = ((IRawGraphClient)client).ExecuteGetCypherResults<Kompanija>(queryCreateSaradjivao).ToList();
            }



            var queryIznajmio= new Neo4jClient.Cypher.CypherQuery("MATCH(k: Klijent{ lk:'"+lkKlijenta+
                "'})-[r]->(a:Automobil{registracija:'"+registracijaAutomobila+"'}) RETURN k", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Klijent> klijenti = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(queryIznajmio).ToList();

            if (klijenti.Count == 0)
            {
                var queryCreateIznajmio = new Neo4jClient.Cypher.CypherQuery("match(k:Klijent{lk:'"
                               + lkKlijenta + "'}) match (n:Automobil{kompanija:'"
                               + kompanijaAutomobila + "', registracija:'"
                               +registracijaAutomobila+"'}) CREATE(k)-[r:Iznajmio]->(n) return k",
                    dict, CypherResultMode.Set);

                List<Klijent> klijent = ((IRawGraphClient)client).ExecuteGetCypherResults<Klijent>(queryCreateIznajmio).ToList();

            }


            MessageBox.Show("Uspesno dodavanje.");

            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            button7.Enabled = true;

            string t = listBox2.SelectedItem.ToString();

            string[] tPodeljeno = t.Split(',');

            textBox3.Text = tPodeljeno[1].ToString();
            textBox4.Text = tPodeljeno[2].ToString();

            button6.Enabled = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {

            string t = listBox2.SelectedItem.ToString();
            string[] tPodeljeno = t.Split(',');
            string registracijaAuta = tPodeljeno[0].ToString();
            //string od = tPodeljeno[1].ToString();
            //string doDatuma = tPodeljeno[2].ToString();
            string kompanijaAutomobila = tPodeljeno[3].ToString();
            string imeKlijenta = tPodeljeno[4].ToString();
            string prezimeKlijenta = tPodeljeno[5].ToString();
            string lkKlijenta = tPodeljeno[6].ToString();

            //string rezervacijaOd = textBox3.Text;
            //string rezervacijaDo = textBox4.Text;

            string od = textBox3.Text.ToString();
            string doDatuma = textBox4.Text.ToString();


            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("doDatuma", doDatuma);
            dict.Add("imeKlijenta", imeKlijenta);
            dict.Add("prezimeKlijenta", prezimeKlijenta);
            dict.Add("imeKompanije", kompanijaAutomobila);
            dict.Add("lkKlijenta", lkKlijenta);
            dict.Add("od", od);
            dict.Add("registracijaAuta", registracijaAuta);


            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Rezervacija {lkKlijenta:'" + lkKlijenta + "', imeKompanije:'" + kompanijaAutomobila + "', registracijaAuta:'" + registracijaAuta + "'  }) "
                                                          + "SET n.od = '" + od + "', n.doDatuma= '" + doDatuma + "'  RETURN n",
                                                                dict, CypherResultMode.Set);

            List<Rezervacija> rezervacije = ((IRawGraphClient)client).ExecuteGetCypherResults<Rezervacija>(query).ToList();

            MessageBox.Show("Uspesna izmena.");

            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;



        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
           if(listBox2.SelectedIndex != -1)
            {
                button6.Enabled = true;
            }
          
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button2.Enabled = true;
            }
        }
    }
}
