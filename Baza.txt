

CREATE (Jovanjica:Kompanija {ime:'Jovanjica', adresa:'Nova Pazova', telefon:'0643526467'})
CREATE (AudiJovanjica:Automobil {registracija:'1', marka:'Audi', model:'A1', proizveden:2003, boja:'crna', dostupan:'Ne', kompanija:'Jovanjica'})
CREATE (MerzedesJovanjica:Automobil {registracija:'12', marka:'Merzedes', model:'GKlasse', proizveden:2015, boja:'crna', dostupan:'Ne', kompanija:'Jovanjica'})
CREATE (FiatJovanjica:Automobil {registracija:'13', marka:'Fiat', model:'Bravo', proizveden:1998, boja:'crvena', dostupan:'Ne', kompanija:'Jovanjica'})
CREATE (RenoJovanjica:Automobil {registracija:'14', marka:'Reno', model:'Stilo', proizveden:2009, boja:'siva', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (OpelJovnjica:Automobil {registracija:'15', marka:'Opel', model:'Astra', proizveden:2013, boja:'plava', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Fiat1Jovanjica:Automobil {registracija:'16', marka:'Fiat', model:'Punto', proizveden:2004, boja:'zuta', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (VolkswagenJovanjica:Automobil {registracija:'17', marka:'Volkswagen', model:'Polo', proizveden:2009, boja:'crna', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (PeugeotJovanjica:Automobil {registracija:'18', marka:'Peugeot', model:'308', proizveden:2005, boja:'plava', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Fiat2Jovanjica:Automobil {registracija:'10', marka:'Fiat', model:'Bravo', proizveden:2007, boja:'crvena', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Opel1Jovanjica:Automobil {registracija:'110', marka:'Opel', model:'Sedan', proizveden:2018, boja:'crna', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Opel2Jovanjica:Automobil {registracija:'111', marka:'Opel', model:'Insignia', proizveden:2015, boja:'siva', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Volkswagen1Jovanjica:Automobil {registracija:'112', marka:'Volkswagen', model:'Golf2', proizveden:1993, boja:'crvena', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Peugeot1Jovanjica:Automobil {registracija:'113', marka:'Peugeot', model:'206', proizveden:2005, boja:'siva', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Merzedes1Jovanjica:Automobil {registracija:'114', marka:'Merzedes', model:'AKlasse', proizveden:1993, boja:'crna', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (SkodaJovanjica:Automobil {registracija:'115', marka:'Skoda', model:'Superb', proizveden:2018, boja:'plava', dostupan:'Da', kompanija:'Jovanjica'})
CREATE (Skoda1Jovanjica:Automobil {registracija:'167', marka:'Skoda', model:'Octavia', proizveden:2009, boja:'crvena', dostupan:'Da', kompanija:'Jovanjica'})
CREATE
(Jovanjica)-[:POSEDUJE]->(AudiJovanjica),
(Jovanjica)-[:POSEDUJE]->(MerzedesJovanjica),
(Jovanjica)-[:POSEDUJE]->(FiatJovanjica),
(Jovanjica)-[:POSEDUJE]->(RenoJovanjica),
(Jovanjica)-[:POSEDUJE]->(OpelJovanjica),
(Jovanjica)-[:POSEDUJE]->(Fiat1Jovanjica),
(Jovanjica)-[:POSEDUJE]->(VolkswagenJovanjica),
(Jovanjica)-[:POSEDUJE]->(PeugeotJovanjica),
(Jovanjica)-[:POSEDUJE]->(Fiat2Jovanjica),
(Jovanjica)-[:POSEDUJE]->(Opel1Jovanjica),
(Jovanjica)-[:POSEDUJE]->(Opel2Jovanjica),
(Jovanjica)-[:POSEDUJE]->(Volkswagen1Jovanjica),
(Jovanjica)-[:POSEDUJE]->(Peugeot1Jovanjica),
(Jovanjica)-[:POSEDUJE]->(Merzedes1Jovanjica),
(Jovanjica)-[:POSEDUJE]->(SkodaJovanjica),
(Jovanjica)-[:POSEDUJE]->(Skoda1Jovanjica)

CREATE (Krusik:Kompanija {ime:'Krusik', adresa:'Valjevo', telefon:'0635357686'})
CREATE (AudiKrusik:Automobil {registracija:'221', marka:'Audi', model:'A1', proizveden:2003, boja:'crna', dostupan:'Ne', kompanija:'Krusik'})
CREATE (MerzedesKrusik:Automobil {registracija:'222', marka:'Merzedes', model:'GKlasse', proizveden:2015, boja:'crna', dostupan:'Ne', kompanija:'Krusik'})
CREATE (FiatKrusik:Automobil {registracija:'223', marka:'Fiat', model:'Bravo', proizveden:1998, boja:'crvena', dostupan:'Ne', kompanija:'Krusik'})
CREATE (RenoKrusik:Automobil {registracija:'224', marka:'Reno', model:'Stilo', proizveden:2009, boja:'siva', dostupan:'Da', kompanija:'Krusik'})
CREATE (OpelKrusik:Automobil {registracija:'225', marka:'Opel', marka:'Opel', model:'Astra', proizveden:2013, boja:'plava', dostupan:'Da', kompanija:'Krusik'})
CREATE (Fiat1Krusik:Automobil {registracija:'226', marka:'Fiat', model:'Punto', proizveden:2004, boja:'zuta', dostupan:'Da', kompanija:'Krusik'})
CREATE (VolkswagenKrusik:Automobil {registracija:'227', marka:'Volkswagen', model:'Polo', proizveden:2009, boja:'crna', dostupan:'Da', kompanija:'Krusik'})
CREATE (PeugeotKrusik:Automobil {registracija:'228', marka:'Peugeot', model:'308', proizveden:2005, boja:'plava', dostupan:'Da', kompanija:'Krusik'})
CREATE (Fiat2Krusik:Automobil {registracija:'229', marka:'Fiat', model:'Bravo', proizveden:2007, boja:'crvena', dostupan:'Da', kompanija:'Krusik'})
CREATE (Opel1Krusik:Automobil {registracija:'230', marka:'Opel', model:'Sedan', proizveden:2018, boja:'crna', dostupan:'Da', kompanija:'Krusik'})
CREATE (Opel2Krusik:Automobil {registracija:'231', marka:'Opel', model:'Insignia', proizveden:2015, boja:'siva', dostupan:'Da', kompanija:'Krusik'})
CREATE (Volkswagen1Krusik:Automobil {registracija:'232', marka:'Volkswagen', model:'Golf2', proizveden:1993, boja:'crvena', dostupan:'Da', kompanija:'Krusik'})
CREATE (Peugeot1Krusik:Automobil {registracija:'233', marka:'Peugeot', model:'206', proizveden:2005, boja:'siva', dostupan:'Da', kompanija:'Krusik'})
CREATE (Merzedes1Krusik:Automobil {registracija:'234', marka:'Merzedes', model:'AKlasse', proizveden:1993, boja:'crna', dostupan:'Da', kompanija:'Krusik'})
CREATE (Skoda:Automobil {registracija:'235', marka:'Skoda', model:'Superb', proizveden:2018, boja:'plava', dostupan:'Da', kompanija:'Krusik'})
CREATE (Skoda1Krusik:Automobil {registracija:'236', marka:'Skoda', model:'Octavia', proizveden:2009, boja:'crvena', dostupan:'Da', kompanija:'Krusik'})
CREATE
(Krusik)-[:POSEDUJE]->(AudiKrusik),
(Krusik)-[:POSEDUJE]->(MerzedesKrusik),
(Krusik)-[:POSEDUJE]->(FiatKrusik),
(Krusik)-[:POSEDUJE]->(RenoKrusik),
(Krusik)-[:POSEDUJE]->(OpelKrusik),
(Krusik)-[:POSEDUJE]->(Fiat1Krusik),
(Krusik)-[:POSEDUJE]->(VolkswagenKrusik),
(Krusik)-[:POSEDUJE]->(PeugeotKrusik),
(Krusik)-[:POSEDUJE]->(Fiat2Krusik),
(Krusik)-[:POSEDUJE]->(Opel1Krusik),
(Krusik)-[:POSEDUJE]->(Opel2Krusik),
(Krusik)-[:POSEDUJE]->(Volkswagen1Krusik),
(Krusik)-[:POSEDUJE]->(Peugeot1Krusik),
(Krusik)-[:POSEDUJE]->(Merzedes1Krusik),
(Krusik)-[:POSEDUJE]->(SkodaKrusik),
(Krusik)-[:POSEDUJE]->(Skoda1Krusik)

CREATE (GIM:Kompanija {ime:'GIM', adresa:'Beograd', telefon:'0664456252'})
CREATE (AudiGIM:Automobil {registracija:'31', marka:'Audi', model:'A1', proizveden:2003, boja:'crna', dostupan:'Ne', kompanija:'GIM'})
CREATE (MerzedesGIM:Automobil {registracija:'32', marka:'Merzedes', model:'GKlasse', proizveden:2015, boja:'crna', dostupan:'Ne', kompanija:'GIM'})
CREATE (FiatGIM:Automobil {registracija:'33', marka:'Fiat', model:'Bravo', proizveden:1998, boja:'crvena', dostupan:'Da', kompanija:'GIM'})
CREATE (RenoGIM:Automobil {registracija:'34', marka:'Reno', model:'Stilo', proizveden:2009, boja:'siva', dostupan:'Da', kompanija:'GIM'})
CREATE (OpelGIM:Automobil {registracija:'35', marka:'Opel', marka:'Opel', model:'Astra', proizveden:2013, boja:'plava', dostupan:'Da'})
CREATE (Fiat1GIM:Automobil {registracija:'36', marka:'Fiat', model:'Punto', proizveden:2004, boja:'zuta', dostupan:'Da', kompanija:'GIM'})
CREATE (VolkswagenGIM:Automobil {registracija:'37', marka:'Volkswagen', model:'Polo', proizveden:2009, boja:'crna', dostupan:'Da', kompanija:'GIM'})
CREATE (PeugeotGIM:Automobil {registracija:'38', marka:'Peugeot', model:'308', proizveden:2005, boja:'plava', dostupan:'Da', kompanija:'GIM'})
CREATE (Fiat2GIM:Automobil {registracija:'39', marka:'Fiat', model:'Bravo', proizveden:2007, boja:'crvena', dostupan:'Da', kompanija:'GIM'})
CREATE (Opel1GIM:Automobil {registracija:'310', marka:'Opel', model:'Sedan', proizveden:2018, boja:'crna', dostupan:'Da', kompanija:'GIM'})
CREATE (Opel2GIM:Automobil {registracija:'311', marka:'Opel', model:'Insignia', proizveden:2015, boja:'siva', dostupan:'Da', kompanija:'GIM'})
CREATE (Volkswagen1GIM:Automobil {registracija:'312', marka:'Volkswagen', model:'Golf2', proizveden:1993, boja:'crvena', dostupan:'Da', kompanija:'GIM'})
CREATE (Peugeot1GIM:Automobil {registracija:'313', marka:'Peugeot', model:'206', proizveden:2005, boja:'siva', dostupan:'Da', kompanija:'GIM'})
CREATE (Merzedes1GIM:Automobil {registracija:'314', marka:'Merzedes', model:'AKlasse', proizveden:1993, boja:'crna', dostupan:'Da', kompanija:'GIM'})
CREATE (SkodaGIM:Automobil {registracija:'315', marka:'Skoda', model:'Superb', proizveden:2018, boja:'plava', dostupan:'Da', kompanija:'GIM'})
CREATE (Skoda1GIM:Automobil {registracija:'316', marka:'Skoda', model:'Octavia', proizveden:2009, boja:'crvena', dostupan:'Da', kompanija:'GIM'})
CREATE
(GIM)-[:POSEDUJE]->(AudiGIM),
(GIM)-[:POSEDUJE]->(MerzedesGIM),
(GIM)-[:POSEDUJE]->(FiatGIM),
(GIM)-[:POSEDUJE]->(RenoGIM),
(GIM)-[:POSEDUJE]->(OpelGIM),
(GIM)-[:POSEDUJE]->(Fiat1GIM),
(GIM)-[:POSEDUJE]->(VolkswagenGIM),
(GIM)-[:POSEDUJE]->(PeugeotGIM),
(GIM)-[:POSEDUJE]->(Fiat2GIM),
(GIM)-[:POSEDUJE]->(Opel1GIM),
(GIM)-[:POSEDUJE]->(Opel2GIM),
(GIM)-[:POSEDUJE]->(Volkswagen1GIM),
(GIM)-[:POSEDUJE]->(Peugeot1GIM),
(GIM)-[:POSEDUJE]->(Merzedes1GIM),
(GIM)-[:POSEDUJE]->(SkodaGIM),
(GIM)-[:POSEDUJE]->(Skoda1GIM)

CREATE (Nebojsa:Klijent {ime:'Nebojsa', prezime:'Tatic', grad:'Beograd', telefon:'0664456252', lk:'007440022198'})
CREATE (Nikola:Klijent {ime:'Nikola', prezime:'Selakovic', grad:'Beograd', telefon:'0664456252', lk:'0078352323221'})
CREATE (Aleksandar:Klijent {ime:'Aleksandar', prezime:'Gavrilovic', grad:'Beograd', telefon:'0664567880', lk:'0089436785432'})
CREATE (Jovan:Klijent {ime:'Jovan', prezime:'Deretic', grad:'Kragujevac', telefon:'062246000', lk:'0034622065445'})

CREATE (Prva:Rezervacija {registracijaAuta:'221', od:'06-Jan-19', doDatuma:'08-Jan-19', imeKompanije:'Krusik', imeKlijenta:'Nebojsa', prezimeKlijenta:'Tatic',lkKlijenta:'007440022198', telKlijenta:'0664456252'})
CREATE (Druga:Rezervacija {registracijaAuta:'31', od:'09-Jan-19', doDatuma:'10-Jan-19', imeKompanije:'GIM', imeKlijenta:'Nebojsa', prezimeKlijenta:'Tatic',lkKlijenta:'007440022198', telKlijenta:'0664456252'})
CREATE (Treca:Rezervacija {registracijaAuta:'1', od:'12-Jan-19', doDatuma:'16-Jan-19', imeKompanije:'Jovanjica', imeKlijenta:'Nikola', prezimeKlijenta:'Selakovic',lkKlijenta:'0078352323221', telKlijenta:'0664456252'})
CREATE (Cetvrta:Rezervacija {registracijaAuta:'13', od:'06-Jan-19', doDatuma:'08-Jan-19', imeKompanije:'Jovanjica', imeKlijenta:'Nikola', prezimeKlijenta:'Selakovic',lkKlijenta:'0078352323221', telKlijenta:'0664456252'})
CREATE (Peta:Rezervacija {registracijaAuta:'222', od:'26-Jan-19', doDatuma:'29-Jan-19', imeKompanije:'Krusik', imeKlijenta:'Aleksandar', prezimeKlijenta:'Gavrilovic',lkKlijenta:'0089436785432', telKlijenta:'0664567880'})
CREATE (Sesta:Rezervacija {registracijaAuta:'12', od:'26-Jan-19', doDatuma:'30-Jan-19', imeKompanije:'Jovanjica', imeKlijenta:'Aleksandar', prezimeKlijenta:'Gavrilovic',lkKlijenta:'0089436785432', telKlijenta:'0664567880'})
CREATE (Sedma:Rezervacija {registracijaAuta:'223', od:'17-Jan-19', doDatuma:'26-Jan-19', imeKompanije:'Krusik', imeKlijenta:'Jovan', prezimeKlijenta:'Deretic',lkKlijenta:'0034622065445', telKlijenta:'062246000'})
CREATE (Osma:Rezervacija {registracijaAuta:'32', od:'18-Jan-19', doDatuma:'26-Jan-19', imeKompanije:'GIM', imeKlijenta:'Jovan', prezimeKlijenta:'Deretic',lkKlijenta:'0034622065445', telKlijenta:'062246000'})

CREATE
(Nebojsa)-[:IZNAJMIO]->(MerzedesKrusik),
(Nebojsa)-[:IZNAJMIO]->(FiatGIM),
(Nikola)-[:IZNAJMIO]->(Opel1Jovanjica),
(Nikola)-[:IZNAJMIO]->(AudiJovanjica),
(Aleksandar)-[:IZNAJMIO]->(OpelKrusik),
(Aleksandar)-[:IZNAJMIO]->(SkodaJovanjica),
(Jovan)-[:IZNAJMIO]->(PeugeotKrusik),
(Jovan)-[:IZNAJMIO]->(Fiat2GIM)

CREATE
(Krusik)-[:SARADJIVAO]->(Nebojsa),
(GIM)-[:SARADJIVAO]->(Nebojsa),
(Jovanjica)-[:SARADJIVAO]->(Nikola),
(Krusik)-[:SARADJIVAO]->(Aleksandar),
(Jovanjica)-[:SARADJIVAO]->(Aleksandar),
(Krusik)-[:SARADJIVAO]->(Jovan),
(GIM)-[:SARADJIVAO]->(Jovan)


CREATE
(Nebojsa)-[:REZERVISE]->(Prva),
(Nebojsa)-[:REZERVISE]->(Druga),
(Nikola)-[:REZERVISE]->(Treca),
(Nikola)-[:REZERVISE]->(Cetvrta),
(Aleksandar)-[:REZERVISE]->(Peta),
(Aleksandar)-[:REZERVISE]->(Sesta),
(Jovan)-[:REZERVISE]->(Sedma),
(Jovan)-[:REZERVISE]->(Osma)


CREATE
(Prva)-[:IZVRSAVA]->(MerzedesKrusik),
(Druga)-[:IZVRSAVA]->(FiatGIM),
(Treca)-[:IZVRSAVA]->(Opel1Jovanjica),
(Cetvrta)-[:IZVRSAVA]->(AudiJovanjica),
(Peta)-[:IZVRSAVA]->(OpelKrusik),
(Sesta)-[:IZVRSAVA]->(SkodaJovanjica),
(Sedma)-[:IZVRSAVA]->(PeugeotKrusik),
(Osma)-[:IZVRSAVA]->(Fiat2GIM)


CREATE
(Prva)-[:PRAVI]->(Krusik),
(Druga)-[:PRAVI]->(GIM),
(Treca)-[:PRAVI]->(Jovanjica),
(Cetvrta)-[:PRAVI]->(Jovanjica),
(Peta)-[:PRAVI]->(Krusik),
(Sesta)-[:PRAVI]->(Jovanjica),
(Sedma)-[:PRAVI]->(Krusik),
(Osma)-[:PRAVI]->(GIM)


